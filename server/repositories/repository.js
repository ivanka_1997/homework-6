const fs = require('fs');
const path = require('path');

class Data {
  static getData(repoPath) {
    const rawData = fs.readFileSync(path.join(__dirname, repoPath));
    const data = JSON.parse(rawData);
    console.log(data);
    return data;
  }

  static setData(addedData, repoPath) {
    if (addedData) {
      let data = Data.getData(repoPath);
      data.push(addedData);

      fs.writeFileSync(path.join(__dirname, repoPath), JSON.stringify(
        data, 
        (k, v) => Array.isArray(k) && !(v = v.filter(e => e)).length ? void 0 : v, 2, 
        (err) => { if(err) throw err; })); 
      return data;
    } else {
      return false;
    }
  }

  static updateData(updatedData, repoPath) {
    if(updatedData) {
      const { id } = updatedData;
      let data = Data.getData(repoPath);
      const dataItemIndex = data.findIndex(dataItem => dataItem.id === id);
      data[dataItemIndex] = updatedData;
      fs.writeFileSync(path.join(__dirname, repoPath), JSON.stringify(
        data, 
        (k, v) => Array.isArray(k) && !(v = v.filter(e => e)).length ? void 0 : v, 2, 
        (err) => { if(err) throw err; }));
      return data;
    } else {
      return false;
    }
  }

  static deleteData(id, repoPath) {
    if(id) {
      let data = Data.getData(repoPath).filter(message => !(message.id === id));
      fs.writeFileSync(path.join(__dirname, repoPath), JSON.stringify(
        data, 
        (k, v) => Array.isArray(k) && !(v = v.filter(e => e)).length ? void 0 : v, 2, 
        (err) => { if(err) throw err; }));
      return data;
    } else {
      return false;
    }
  }
}

module.exports = {
  Data
};