const { Data } = require("../repositories/repository");

class ChatService {
  static getMessages() {
    const allMessages = Data.getData("./messagelist.json");
    return {
      message: "GET. Get all messages",
      data: allMessages,
    };
  }

  static getMessage(messageId) {
    const message = ChatService.getMessages().data.find(
      (message) => message.id === messageId
    );
    console.log(message, "message");
    if (!message) {
      return {
        error: "Can not GET. Message with current id doesn`t exist",
      };
    } else {
      return {
        message: "GET. Get message by id",
        data: message,
      };
    }
  }

  static addMessage(message) {
    console.log(message);
    const { id } = message;

    if (ChatService.getMessage(id).data) {
      return {
        error: "Can not POST. Message with current id exists",
      };
    } else {
      Data.setData(message, "./messagelist.json");
      return {
        message: "POST. Create new message",
      };
    }
  }

  static updateMessage(message, updatedId) {
    const { id } = message;
    if (updatedId != id) {
      return {
        error:
          "Can not PUT. Ids in the query string and in the object don`t match",
      };
    } else if (ChatService.getMessage(updatedId).error) {
      // Data.setData(user, './messagelist.json');
      // return {
      //   message: 'PUT. Create new message',
      //   code: 201
      // };
    } else {
      Data.updateData(message, "./messagelist.json");
      return {
        message: "PUT. Edit existing message",
        code: 200,
      };
    }
  }

  static deleteMessage(id) {
    if (ChatService.getMessage(id).error) {
      return {
        error: "Can not DELETE. Message with current id doesn`t exist",
      };
    } else {
      Data.deleteData(id, "./messagelist.json");
      return {
        message: "DELETE. Delete existing message",
      };
    }
  }
}

module.exports = {
  ChatService,
};
