const { Data } = require("../repositories/repository");

class UserService {
  static getUsers() {
    const allUsers = Data.getData("./userlist.json");
    return {
      message: "GET. Get all users",
      data: allUsers,
    };
  }

  static getUser(userId) {
    console.log(userId - 1);
    const user = UserService.getUsers().data[userId - 1];
    console.log(user, "User");
    if (!user) {
      return {
        error: "Can not GET. User with current id doesn`t exist",
      };
    } else {
      return {
        message: "GET. Get user by id",
        data: user,
      };
    }
  }

  static addUser(user) {
    const { _id } = user;
    console.log(_id);
    if (_id <= 0 || isNaN(_id)) {
      return {
        error: "Can not POST. Wrong id number",
      };
    } else if (UserService.getUser(Number(_id)).data) {
      return {
        error: "Can not POST. User with current id exists",
      };
    } else {
      Data.setData(user);
      return {
        message: "POST. Create new user",
      };
    }
  }

  static updateUser(user, id) {
    const { _id } = user;
    if (_id <= 0 || id <= 0 || isNaN(_id) || isNaN(id)) {
      return {
        error: "Can not PUT. Wrong id number",
      };
    } else if (id != _id) {
      return {
        error:
          "Can not PUT. Ids in the query string and in the object don`t match",
      };
    } else if (UserService.getUser(id).error) {
      Data.setData(user);
      return {
        message: "PUT. Create new user",
        code: 201,
      };
    } else {
      Data.updateData(user);
      return {
        message: "PUT. Edit existing user",
        code: 200,
      };
    }
  }

  static deleteUser(id) {
    if (UserService.getUser(id).error) {
      return {
        error: "User with such id doesn`t exist",
      };
    } else {
      Data.deleteData(id);
      return {
        message: "DELETE. Delete existing user",
      };
    }
  }
}

module.exports = {
  UserService,
};
