const express = require("express");
const router = express.Router();

const { UserService } = require("../services/user.service");

router.get("/", (req, res, next) => {
  const result = UserService.getUsers();
  res.status(200).json(result.data);
});

router.get("/:id", (req, res, next) => {
  const id = Number(req.params.id);
  const result = UserService.getUser(id);
  if (result.error) {
    res.status(404).send(result.error);
  } else {
    res.status(200).json(result.data);
  }
});

router.post("/", (req, res, next) => {
  console.log(req.body);
  const result = UserService.addUser(req.body);

  if (result.error) {
    res.status(400).send(result.error);
  } else {
    res.status(201).send(result.message);
  }
});

router.put("/:id", (req, res, next) => {
  const id = Number(req.params.id);
  const result = UserService.updateUser(req.body, id);

  if (result.error) {
    res.status(400).send(result.error);
  } else {
    res.status(result.code).send(result.message);
  }
});

router.delete("/:id", (req, res, next) => {
  const id = Number(req.params.id);
  const result = UserService.deleteUser(id);

  if (result.error) {
    res.status(400).send(result.error);
  } else {
    res.status(200).send(result.message);
  }
});

module.exports = router;
