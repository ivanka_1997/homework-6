import styled from "styled-components";

const StyledParagraph = styled.p`
  margin: 5px;
  font-size: 16px;
  &.messageTime {
    font-size: 12px;
    color: gray;
  }
`;

export default StyledParagraph;
