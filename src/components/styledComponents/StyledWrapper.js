import styled from "styled-components";

const StyledWrapper = styled.div`
  display: flex;
  flex-direction: column;
  &.icon_wrapp {
    align-items: center;
  }
`;

export default StyledWrapper;
