import styled from "styled-components";

const StyledInputWrapper = styled.div`
  border-radius: 5px;
  box-shadow: 0 14px 28px rgba(0, 0, 0, 0.25), 0 10px 10px rgba(0, 0, 0, 0.22);
  width: 100%;
  margin-top: 10px;
  display: flex;
  height: 150px;
  justify-content: space-around;
`;

export default StyledInputWrapper;
