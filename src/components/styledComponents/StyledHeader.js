import styled from "styled-components";

const StyledHeader = styled.header`
  width: 100%;
  height: 50px;
  display: flex;
  justify-content: space-between;
`;
export default StyledHeader;
