import styled from "styled-components";

const StyledMessageWrapper = styled.div`
  border: 2px solid red;
  border-radius: 5px;
  padding: 5px;
  width: fit-content;
  margin-top: 10px;
  display: flex;
  &.right {
    margin: 10px 0 auto auto;
    justify-content: space-between;
  }
`;

export default StyledMessageWrapper;
