import {
  FETCH_MESSAGES,
  FETCH_MESSAGE,
  ADD_MESSAGE,
  UPDATE_MESSAGE,
  DELETE_MESSAGE,
} from "../constants/message";

const fetchMessagesAction = () => ({ type: FETCH_MESSAGES });

const fetchMessageAction = (id) => ({ type: FETCH_MESSAGE, id });

const addMessageAction = (message) => ({ type: ADD_MESSAGE, message });

const editMessageAction = (message) => ({ type: UPDATE_MESSAGE, message });

const deleteMessageAction = (id) => ({ type: DELETE_MESSAGE, id });

export {
  fetchMessagesAction,
  fetchMessageAction,
  addMessageAction,
  editMessageAction,
  deleteMessageAction,
};
