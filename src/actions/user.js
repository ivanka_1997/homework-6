import {
  FETCH_USERS,
  ADD_USER,
  UPDATE_USER,
  DELETE_USER,
} from "../constants/user";

const fetchUsersAction = () => ({ type: FETCH_USERS });

const addUserAction = (user) => ({ type: ADD_USER, user });

const updateUserAction = (user) => ({ type: UPDATE_USER, user });

const deleteUserAction = (id) => ({ type: DELETE_USER, id });

export { fetchUsersAction, addUserAction, updateUserAction, deleteUserAction };
