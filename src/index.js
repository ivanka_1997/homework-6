import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./views/App";
import configureStore from "./store";
import rootSaga from "./sagas/sagas";
import { Provider } from "react-redux";
import { Router, Route } from "react-router-dom";
import { createBrowserHistory } from "history";
// import * as serviceWorker from "./serviceWorker";
const history = createBrowserHistory();

const store = configureStore();
store.runSaga(rootSaga);

ReactDOM.render(
  <Provider store={store}>
    <Router history={history}>
      <Route exact path="/" component={App} />
    </Router>
  </Provider>,
  document.getElementById("root")
);
