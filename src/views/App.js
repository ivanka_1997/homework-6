import React from "react";
import Chat from "../Chat";
import EditModal from "../components/EditModal";
import { Switch, Route } from "react-router-dom";
import UserList from "../users/index";
import UserPage from "../userPage/index";

const App = (props) => {
  return (
    <div>
      <Switch>
        <Route exact path="/" component={Chat} />
        <Route exact path="/message/:id" component={EditModal} />
        <Route exact path="/user" component={UserList} />
        <Route path="/user/:id" component={UserPage} />
      </Switch>
    </div>
  );
};

export default App;
