import React, { Component } from "react";
import { connect } from "react-redux";
import moment from "moment";
import * as actions from "./actions/message";
import {
  fetchMessagesAction,
  deleteMessageAction,
  editMessageAction,
} from "./actions/message";
import Header from "./components/Header";
import EditModal from "./components/EditModal";
import Message from "./components/Message";
import Input from "./components/Input";
import { css } from "@emotion/core";
import ClipLoader from "react-spinners/ClipLoader";

// Can be a string as well. Need to ensure each key-value pair ends with ;
const override = css`
  display: block;
  margin: auto;
  margin-top: 15%;
  border-color: red;
`;

class Chat extends Component {
  componentDidMount() {
    this.props.fetchMessagesAction();
    console.log(this.props);
  }

  sortMessages() {
    console.log(this.props.messages);
    const messages = this.props.messages;
    return messages.sort((first, second) =>
      moment(first.createdAt).diff(moment(second.createdAt))
    );
  }

  deleteMessage = (id) => {
    this.props.deleteMessageAction(id);
  };

  editMessage = () => {
    const message = this.props.message;
    this.props.showPageAction();
    this.props.setEditedMessageAction(message);
  };

  render() {
    const messages = this.sortMessages();
    if (!this.props.fetchMessagesLoading)
      return (
        <ClipLoader
          color="red"
          loading={this.props.fetchMessagesLoading}
          css={override}
          size={150}
        />
      );

    return (
      <>
        <Header messages={messages} />
        {messages.map(({ avatar, text, createdAt, id }) => (
          <Message
            lastMessageId={messages[messages.length - 1].id}
            userAvatar={avatar}
            messageText={text}
            messageTime={createdAt}
            message_id={id}
            key={id}
            deleteMessage={this.deleteMessage}
            editMessage={this.editMessage}
          />
        ))}
        <Input />
        {this.props.isShown ? <EditModal message={this.props.message} /> : null}
      </>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    messages: state.messages,
    fetchMessagesLoading: state.fetchMessagesLoading,
    fetchMessagesError: state.fetchMessagesError,
  };
};

const mapDispatchToProps = {
  fetchMessagesAction,
  deleteMessageAction,
  editMessageAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
