import {
  FETCH_USERS,
  FETCH_USERS_SUCCESS,
  FETCH_USERS_FAILED,
  ADD_USER,
  ADD_USER_SUCCESS,
  ADD_USER_FAILED,
  UPDATE_USER,
  UPDATE_USER_SUCCESS,
  UPDATE_USER_FAILED,
  DELETE_USER,
  DELETE_USER_SUCCESS,
  DELETE_USER_FAILED,
} from "../constants/user";

const initialState = {
  users: [],
  fetchUsersLoading: true,
  fetchUsersError: false,
  addUserLoading: true,
  addUserError: false,
  updateUserLoading: true,
  updateUserError: false,
  deleteUserLoading: true,
  deleteUserError: false,
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_USERS:
      return Object.assign({}, state, {
        users: [],
        fetchUsersLoading: true,
        fetchUsersError: false,
      });
    case FETCH_USERS_SUCCESS:
      return Object.assign({}, state, {
        users: action.users,
        fetchUsersLoading: false,
      });
    case FETCH_USERS_FAILED:
      return Object.assign({}, state, { fetchUsersError: true });
    case ADD_USER:
      return Object.assign({}, state, {
        users: [],
        addUserLoading: true,
        addUserError: false,
      });
    case ADD_USER_SUCCESS:
      return Object.assign({}, state, { addUserLoading: false });
    case ADD_USER_FAILED:
      return Object.assign({}, state, { addUserError: true });
    case UPDATE_USER:
      return Object.assign({}, state, {
        users: [],
        updateUserLoading: true,
        updateUserError: false,
      });
    case UPDATE_USER_SUCCESS:
      return Object.assign({}, state, { updateUserLoading: false });
    case UPDATE_USER_FAILED:
      return Object.assign({}, state, { updateUserError: true });
    case DELETE_USER:
      return Object.assign({}, state, {
        users: [],
        deleteUserLoading: true,
        deleteUserError: false,
      });
    case DELETE_USER_SUCCESS:
      return Object.assign({}, state, { deleteUserLoading: false });
    case DELETE_USER_FAILED:
      return Object.assign({}, state, { deleteUserError: true });
    default:
      return state;
  }
};

export default userReducer;
